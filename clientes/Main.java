/*
* Integrantes
* Antonio Colmán
 */

package py.una.pol.clientes;

import java.util.*;
import java.net.*;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.*;


public class Main {

    public static void main(String[] args) {
        personasURL();
    }

    public static void personasURL() {

        String myURL = "http://localhost:8080/personas/rest/personas/cedula?cedula=5241779";

        try {
            URL url = new URL(myURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            //conn.setRequestProperty("Content-Type", "application/json");

            if (conn.getResponseCode() < 200 || conn.getResponseCode() >= 300) {
                throw new RuntimeException("Error HTTP - código : " + conn.getResponseCode() + " : " + conn.getResponseMessage());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Impresión del contenido de la respuesta: \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();
        } catch(MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
