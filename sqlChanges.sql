/* Crear tabla persona*/

DROP TABLE matricula CASCADE;

CREATE TABLE persona
(
    cedula integer NOT NULL,
    nombre character varying(1000),
    apellido character varying(1000),
    CONSTRAINT pk_cedula PRIMARY KEY (cedula)
)
WITH (
    OIDS=FALSE
);

/* Crear tabla asignatura*/
CREATE TABLE asignatura
(
    codigo integer NOT NULL,
    nombre character varying(1000),
    profesor character varying(1000),
    CONSTRAINT pk_asignatura PRIMARY KEY (codigo)
)
WITH (
        OIDS=FALSE
);

/* Crear tabla matricula*/
CREATE TABLE matricula
(
    codigo   integer NOT NULL,
    id_p integer,
    id_a integer,
    CONSTRAINT pk_matricula PRIMARY KEY (codigo),
    CONSTRAINT fk_persona FOREIGN KEY (id_p) REFERENCES persona (cedula),
    CONSTRAINT fk_asignatura FOREIGN KEY (id_a) REFERENCES asignatura (codigo)
)
WITH (
        OIDS=FALSE
);
