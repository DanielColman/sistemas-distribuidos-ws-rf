package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;

@Stateless
public class AsignaturaDAO {


    @Inject
    private Logger log;

    /**
     *
     * @param condiciones
     * @return
     */
    public List<Asignatura> seleccionar() {
        String query = "SELECT codigo, nombre, profesor FROM asignatura ORDER BY nombre";

        List<Asignatura> lista = new ArrayList<Asignatura>();

        Connection conn = null;
        try
        {
            conn = Bd.connect();
            ResultSet rs = conn.createStatement().executeQuery(query);

            while(rs.next()) {
                Asignatura a = new Asignatura();
                a.setCodigo(rs.getShort(1));
                a.setNombre(rs.getString(2));
                a.setProfesor(rs.getString(3));

                lista.add(a);
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            }
        }
        return lista;

    }

    public Asignatura seleccionarPorCodigo(short id) {
        String SQL = "SELECT codigo, nombre, profesor FROM asignatura WHERE codigo = ? ";

        Asignatura a = null;

        Connection conn = null;
        try
        {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, id);

            ResultSet rs = pstmt.executeQuery();

            while(rs.next()) {
                a = new Asignatura();
                a.setCodigo(rs.getShort(1));
                a.setNombre(rs.getString(2));
                a.setProfesor(rs.getString(3));
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            }
        }
        return a;

    }


    public long insertar(Asignatura a) throws SQLException {

        String SQL = "INSERT INTO asignatura(codigo, nombre, profesor) " + "VALUES(?,?,?)";

        long id = 0;

        Connection conn = null;

        try
        {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, a.getCodigo());
            pstmt.setString(2, a.getNombre());
            pstmt.setString(3, a.getProfesor());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            }
        }

        return id;


    }


    public long actualizar(Asignatura a) throws SQLException {

        String SQL = "UPDATE asignatura SET nombre = ? , profesor = ? WHERE codigo = ? ";

        long id = 0;
        Connection conn = null;

        try
        {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, a.getNombre());
            pstmt.setString(2, a.getProfesor());
            pstmt.setLong(3, a.getCodigo());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            }
        }
        return id;
    }

    public long borrar(long codigo) throws SQLException {

        String SQL = "DELETE FROM asignatura WHERE codigo = ? ";

        long id = 0;
        Connection conn = null;

        try
        {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, codigo);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    log.severe("Error en la eliminación: " + ex.getMessage());
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            log.severe("Error en la eliminación: " + ex.getMessage());
            throw ex;
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
                throw ef;
            }
        }
        return id;
    }


}
