package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Matricula;
import py.una.pol.personas.model.Persona;

@Stateless
public class MatriculaDAO {


    @Inject
    private Logger log;

    /**
     *
     * @param condiciones
     * @return
     */
    public List<Matricula> seleccionar() {
        String query = "SELECT codigo, id_p, id_a FROM matricula ORDER BY id_a";

        List<Matricula> lista = new ArrayList<Matricula>();

        Connection conn = null;
        try
        {
            conn = Bd.connect();
            ResultSet rs = conn.createStatement().executeQuery(query);

            while(rs.next()) {
                Matricula m = new Matricula();
                m.setCodigo(rs.getInt(1));
                m.setId_a(rs.getInt(2));
                m.setId_p(rs.getInt(3));

                lista.add(m);
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            }
        }
        return lista;

    }

    public Matricula seleccionarPorCodigo(short codigo) {
        String SQL = "SELECT codigo, id_a, id_p FROM matricula WHERE codigo = ? ";

        Matricula m = null;

        Connection conn = null;
        try
        {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, codigo);

            ResultSet rs = pstmt.executeQuery();

            while(rs.next()) {
                m = new Matricula();
                m.setCodigo(rs.getInt(1));
                m.setId_p(rs.getInt(2));
                m.setId_a(rs.getInt(3));
            }

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            }
        }
        return m;

    }


    public long insertar(Matricula m) throws SQLException {

        String SQL = "INSERT INTO matricula(codigo, id_p, id_a) "
                + "VALUES(?,?,?)";

        long id = 0;
        Connection conn = null;

        try
        {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, m.getCodigo());
            pstmt.setInt(2, m.getId_p());
            pstmt.setInt(3, m.getId_a());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            throw ex;
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            }
        }

        return id;


    }


    public long actualizar(Matricula m) throws SQLException {

        String SQL = "UPDATE matricula SET id_p = ? , id_a = ? WHERE codigo = ? ";

        long id = 0;
        Connection conn = null;

        try
        {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, m.getId_p());
            pstmt.setInt(2, m.getId_a());
            pstmt.setInt(3, m.getCodigo());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
            }
        }
        return id;
    }

    public long borrar(short codigo) throws SQLException {

        String SQL = "DELETE FROM matricula WHERE codigo = ? ";

        long id = 0;
        Connection conn = null;

        try
        {
            conn = Bd.connect();
            PreparedStatement pstmt = conn.prepareStatement(SQL);
            pstmt.setLong(1, codigo);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    log.severe("Error en la eliminación: " + ex.getMessage());
                    throw ex;
                }
            }
        } catch (SQLException ex) {
            log.severe("Error en la eliminación: " + ex.getMessage());
            throw ex;
        }
        finally  {
            try{
                conn.close();
            }catch(Exception ef){
                log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
                throw ef;
            }
        }
        return id;
    }


}
