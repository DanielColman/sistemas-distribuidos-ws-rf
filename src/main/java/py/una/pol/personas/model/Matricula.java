package py.una.pol.personas.model;

public class Matricula {

    Integer codigo;
    Integer id_p;
    Integer id_a;

    public Matricula() {

    }

    public Matricula(Integer codigo, Integer id_p, Integer id_a) {
        this.codigo = codigo;
        this.id_p = id_p;
        this.id_a = id_a;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getId_p() {
        return id_p;
    }

    public void setId_p(Integer id_p) {
        this.id_p = id_p;
    }

    public Integer getId_a() {
        return id_a;
    }

    public void setId_a(Integer id_a) {
        this.id_a = id_a;
    }

}
